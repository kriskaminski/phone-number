/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { TConditionMode } from "./mode";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class PhoneNumberCondition extends ConditionBlock<{
    mode: TConditionMode;
}> {
    @condition
    verify(): boolean {
        const phoneNumberSlot = this.valueOf<string>();

        if (phoneNumberSlot) {
            switch (this.props.mode) {
                case "defined":
                    return phoneNumberSlot.string !== "";
                case "undefined":
                    return phoneNumberSlot.string === "";
            }
        }

        return false;
    }
}
